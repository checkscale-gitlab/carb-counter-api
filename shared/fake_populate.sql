INSERT INTO categories
    (id, name)
VALUES
    (1 , 'Fast Food'),
    (2 , 'Healty Food');
    
/* Foods of Fast Food */
INSERT INTO foods
    (id, name, category_id)
VALUES
    (1, 'Hamburger', 1),
    (2, 'Pizza', 1);
    
INSERT INTO nutrition_facts
    (food_id, calorie, carbs, fat, protein)
VALUES
    (1, 10.0, 20.0, 30.0, 40.0),
    (2, 50.0, 60.0, 70.0, 80.0);
    
INSERT INTO exchange_list
    (food_id, vegetable, fruit, fat, milk, meat, starch)
VALUES
    (1, 100.0, 200.0, 300.0, 400.0, 500.0, 600.0),
    (2, 500.0, 600.0, 700.0, 800.0, 900.0, 1000.0);
    
INSERT INTO barcodes
    (food_id, code)
VALUES
    (1, '1111111111111'),
    (2, '2222222222222');
    
/* Foods of Healty Food */
INSERT INTO foods
    (id, name, category_id)
VALUES
    (10, 'Broccoli', 2),
    (11, 'Nuts', 2);
    
INSERT INTO nutrition_facts
    (food_id, calorie, carbs, fat, protein)
VALUES
    (10, 10.05, 20.10, 30.15, 40.20),
    (11, 50.05, 60.10, 70.15, 80.20);
    
INSERT INTO exchange_list
    (food_id, vegetable, fruit, fat, milk, meat, starch)
VALUES
    (10, 100.50, 200.55, 300.60, 400.65, 500.70, 600.75),
    (11, 500.50, 600.55, 700.60, 800.65, 900.70, 1000.75);
    
INSERT INTO barcodes
    (food_id, code)
VALUES
    (10, '1010101010101'),
    (11, '2020202020202');