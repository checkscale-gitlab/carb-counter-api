# Carb Counter API

Carb Counter API lists foods. Recognizes foods from images. Gets their barcode, nutritional facts and exchange list values.

Current Routes
1. GET /foods lists all foods
2. GET /foods/{id} list food with that id
3. POST /files Save image at server. With multipart form. Use photo keyword to upload.
4. GET /vision/{path} Get labels from image at path

# Usage
1. You have to install [docker](https://docs.docker.com/install/) with docker-compose
2. 
```
docker-compose up
```
3. Copy [.env.example](./api/.env.example) to api/.env
4. Change enviromental variables respectivally

# Sample Data
1. Get bash shell of db
```
(host)$ docker-compose exec db bash
```
2. Populate db
```
(docker-db)$ mysql -u carb_counter -pcarb_counter -D carb_counter < /shared/populate.sql
```