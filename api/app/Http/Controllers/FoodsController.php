<?php

namespace App\Http\Controllers;
use App\Food;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FoodsController extends Controller
{
    public function index()
    {
        return response()->json(Food::with([
            'barcode',
            'category',
            'nutritionFacts',
            'exchangeList',
        ])->get(), 200);
    }
    
    public function show($id)
    {
        /* load food with relations */
        $food = Food::with([
            'barcode',
            'category',
            'nutritionFacts',
            'exchangeList',
        ])->find($id);

        if (is_null($food)) {
            return response()->json([
                'msg' => 'Food not exists'
            ], 404);
        }

        return response()->json($food, 200);
    }

    public function showByCode($code)
    {
        /* load food with relations */
        $food_id = DB::table('barcodes')->where('code', $code)->value('food_id');
        
        if (is_null($food_id)) {
            return response()->json([
                'msg' => 'Barcode not exists'
            ], 404);
        }
        
        return $this->show($food_id);
    }
}
