<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FilesController extends Controller
{
    public function create(Request $request)
    {
        $path = $request->file('photo')->storePubliclyAs('pics', 'last');
        if ($path)
        {
            return response()->json(['success' => true], 200);
        }
        return response()->json(['success' => false], 500);
    }
}
