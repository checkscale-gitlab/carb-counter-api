<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;

class CategoriesController extends Controller
{
    public function index()
    {
        return response()->json(Category::all(), 200);
    }

    public function show($id)
    {
        $category = Category::with('foods')->find($id);
        if (is_null($category)) {
            return response()->json([
                'msg' => 'Category not exists'
            ], 404);
        }

        return response()->json($category, 200);
    }
}
