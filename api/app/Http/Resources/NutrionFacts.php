<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class NutrionFacts extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
          'calorie' => $this->calorie,  
          'carbs' => $this->carbs,  
          'fat' => $this->fat,  
          'protein' => $this->protein,  
        ];
    }
}
