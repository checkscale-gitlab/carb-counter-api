<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ExchangeList extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'vegetable' => $this->vegetable,
            'fruit' => $this->fruit,
            'fat' => $this->fat,
            'milk' => $this->milk,
            'meat' => $this->meat,
            'starch' => $this->starch,
        ];
    }
}
