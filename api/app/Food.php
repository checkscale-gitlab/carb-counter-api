<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Food extends Model
{
    protected $hidden = ['created_at', 'updated_at'];
    
    public function nutritionFacts()
    {
        return $this->hasOne('App\NutritionFacts');
    }

    public function exchangeList()
    {
        return $this->hasOne('App\ExchangeList');
    }

    public function barcode()
    {
        return $this->hasOne('App\Barcode');
    }

    public function category()
    {
        return $this->belongsTo('App\Category');
    }
}
