<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExchangeList extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'exchange_list';
    protected $hidden = ['id', 'food_id', 'created_at', 'updated_at'];

    /**
     * Get the food that has this exchange list
     */
    public function food()
    { 
        return $this->belongsTo('App\Food');
    }
}
