<?php

use Illuminate\Http\Request;
use App\Http\Resources\Food as FoodResource;
use App\Food;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/foods', 'FoodsController@index');
Route::get('/foods/{id}', 'FoodsController@show');
Route::get('/foods/barcodes/{code}', 'FoodsController@showByCode');

Route::get('/categories', 'CategoriesController@index');
Route::get('/categories/{id}', 'CategoriesController@show');

Route::post('/upload', 'FilesController@create');
Route::get('/vision', 'VisionController@get');
